<?php
include_once( PATH_SMARTY . 'Smarty.class.php' );

class View
{
	private 	$template;
	private 	$tpl;
	protected 	$user;

	function __construct()
	{
		// Templates class. Smarty:
		$this->template = new Smarty;
		$this->template->compile_check = true;
	}

	public function assign( $name, $value )
	{
		$this->template->assign( $name, $value );
	}

	public function show()
	{
		$this->assign( 'url', array( 'global' => URL_ABSOLUTE ) );
		$this->template->display( $this->tpl  );
	}

	public function setLayout( $template )
	{
		$this->tpl = $template;
	}

	public function fetch()
	{
		$this->assign( 'url', array( 'global' => URL_ABSOLUTE ) );
		return $this->template->fetch( $this->tpl  );
	}
}


?>
